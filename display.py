# Display folder size with depth=1
import os
import datetime
import csv

from config import configs
from database import Session, Container, Folder, Blob
from azure.identity import AzureCliCredential
from azure.storage.blob import BlobServiceClient

LIMIT = configs.get('limit', 20)

def display(account_name, container_name, prefix=None, Session=Session):
    """
    Display the result of specific container/prefix,
    and save the result to file.
    """
    sess = Session()
    container_id = sess.query(Container.id).filter_by(account_name=account_name, name=container_name).first()[0]
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    if not os.path.isdir("results"):
        os.mkdir("results")
    
    if prefix == None:
        root = sess.query(Folder).filter_by(container_id=container_id, path="").first()
        if not root: return
        root_id = root.id
        q = sess.query(Folder).filter_by(container_id=container_id).filter(Folder.parent_id!=root_id).order_by(Folder.size.desc())[:LIMIT]
        q2 = sess.query(Folder).filter_by(container_id=container_id).filter(Folder.parent_id!=root_id).order_by(Folder.blobs_count.desc())[:LIMIT]
    else:
        root = sess.query(Folder).filter_by(container_id=container_id, path=prefix).first()
        if not root: return
        q = sess.query(Folder).filter_by(container_id=container_id, parent_id=root.id).order_by(Folder.size.desc())[:LIMIT]
        q2 = sess.query(Folder).filter_by(container_id=container_id, parent_id=root.id).order_by(Folder.blobs_count.desc())[:LIMIT]

    prefix_name = "_"+prefix.replace('/','_')[:-1] if prefix else ""
    outfile = f"results/{account_name}_{container_name}{prefix_name}_{timestamp}.txt"
    out = open(outfile, "w", encoding="utf8")
    print(f"Storage Usage info of {account_name}/{container_name}/{prefix} on {timestamp}")
    out.write(f"Storage Usage info of {account_name}/{container_name}/{prefix} on {timestamp}\n")
    
    print("Total Size:", root)
    out.write(f"Total Size: {root}\n")
    print("Order by Size:")
    out.write("Order by size:\n")
    for idx, f in enumerate(q):
        print(idx+1, f)
        out.write(f"{idx+1} {f}\n")

    print("\nOrder by Blobs Count:")
    out.write("\nOrder by Blobs Count:\n")
    for idx, f in enumerate(q2):
        print(idx+1, f)
        out.write(f"{idx+1} {f}\n")
    out.close()

def display_sub(sub, acc_sess_sub):
    azure_cli = AzureCliCredential()
    if not os.path.isdir("cli_results"):
        os.mkdir("cli_results")
    if not os.path.isdir("cli_results/csv"):
        os.mkdir("cli_results/csv")
    out = open("cli_results/"+sub+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+".txt", "w", encoding="utf8")
    out.write(f"{sub}:\n\n")
    fcsv = open("cli_results/csv/"+sub+"_"+datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+".csv", "w", newline='', encoding="utf8")
    
    headers = ["Subscription", "Storage Account", "Container", "Path", "Size", "Blobs", "Last Modified", "Completed"]
    out_csv = csv.DictWriter(fcsv, headers)
    out_csv.writeheader()
    row = {"Subscription": sub,
         "Storage Account": None,
         "Container": None,
         "Path": "",
         "Size": 0,
         "Blobs": 0,
         "Last Modified": None,
         "Completed": False
    }
    for account in acc_sess_sub:
        out.write(f"\nStorage Account: {account}\n")
        row["Storage Account"] = account
        blob_srv_client = BlobServiceClient(account, credential=azure_cli)
        account_name = blob_srv_client.account_name
        sess = acc_sess_sub[account]()
        for container in blob_srv_client.list_containers():
            container_name = container.name
            prefix = ""
            container_id = sess.query(Container.id).filter_by(account_name=account_name, name=container_name).first()[0]
            root = sess.query(Folder).filter_by(container_id=container_id, path="").first()
            root_id = root.id if root else None
            q = sess.query(Folder).filter_by(container_id=container_id).filter(Folder.id!=root_id).order_by(Folder.size.desc())[:LIMIT]
            out.write(f"Container: {container_name}\n")
            row["Container"] = container_name
            out.write(f"Total Size: {root}\n")
            if root:
                row["Path"] = root.path
                row["Size"] = root.size
                row["Blobs"] = root.blobs_count
                row["Last Modified"] = root.mtime
                row["Completed"] = root.is_completed
                out_csv.writerow(row)
            if q:
                out.write("Folders Order by size:\n")
                for idx, f in enumerate(q):
                    out.write(f"{idx+1} {f}\n")
                    row["Path"] = f.path
                    row["Size"] = f.size
                    row["Blobs"] = f.blobs_count
                    row["Last Modified"] = f.mtime
                    row["Completed"] = f.is_completed
                    out_csv.writerow(row)
    out.close()
    fcsv.close()