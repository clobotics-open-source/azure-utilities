from database import Blob, Folder, Container, Checkpoint, Session
from database import query_insert_data, scoped_session
from azure.storage.blob import ContainerClient, BlobPrefix, BlobProperties
from loguru import logger
from tqdm import tqdm
from tools import bytes_unit_convert
from config import configs

import concurrent.futures
from functools import partial
from threading import Lock, current_thread

import os
import datetime, time
import pytz

import func_timeout
import pickle

utc = pytz.UTC

MAX_DEPTH = configs['max_depth']
TIMEOUT = configs['timeout']
MAX_RETRY_TIMES = configs['retry_times']
MAX_PAGE_SIZE = configs['page_size']
MAX_WORKERS = configs['max_workers']

Session_local = scoped_session(Session)

mutex = Lock()
err_lock = Lock()
err_count = 0

def add_subfolder(sess, folder, subfolder):
    """
    Update current folder info to database.
    
    Add the subfolder record to current folder.
    """
    with mutex:
        folder.size += subfolder.size
        folder.mtime = subfolder.mtime if subfolder.mtime > folder.mtime else folder.mtime
        folder.blobs_count += subfolder.blobs_count
        sess.commit()

def build_tree(container_client, cid, prefix, Session_local, max_depth=2, resume=True, sub=None):
    """
    build blob tree with a small max_depth

    return: 
        [(prefix, folder_id)]
    """
    sess = Session_local()
    logger.info(f"{container_client.account_name}::{container_client.container_name} building tree")
    sub_path = sub +"/" if sub else ""
    FOLDERS = "pkls/"+sub_path
    if not os.path.isdir(FOLDERS):
        os.makedirs(FOLDERS)
    FOLDERS += container_client.account_name+"_"+container_client.container_name
    FOLDERS += "_"+prefix.replace('/','_')[:-1]+".pkl" if prefix else ".pkl"
    if resume and os.path.isfile(FOLDERS):
        with open(FOLDERS, "rb") as f:
            return pickle.load(f)

    stk = []
    stk.append(container_client.walk_blobs(name_starts_with=prefix))
    folder = Folder(container_id=cid, path=prefix)
    folder = query_insert_data(sess, folder)
    folder.blobs_count = 0
    folder.size = 0
    folder.is_completed = False
    logger.info(f'F: {prefix}')
    depth = 1
    mdepth = max_depth
    result = []
    while stk:
        try:
            item = stk[-1].next()
            if isinstance(item, BlobPrefix):
                if depth == 1:
                    logger.info('F: ' +item.name)
                subfolder = Folder(container_id=cid, path=item.name)
                subfolder = query_insert_data(sess, subfolder)
                subfolder.parent_id=folder.id
                if depth < mdepth:
                    depth += 1
                    folder = subfolder
                    folder.is_completed = False
                    folder.size = 0
                    folder.blobs_count = 0
                    stk.append(item)
                else:
                    if not resume:
                        subfolder.is_completed = False
                        subfolder.size = 0
                        subfolder.blobs_count = 0 
                    result.append(subfolder.id)
            else:
                cnt = 0
                while(isinstance(item, BlobProperties)): 
                    item = stk[-1].next()
                    cnt+=1
                    if cnt >=2500:
                        raise StopIteration
        except StopIteration:
            stk.pop()
            depth -= 1
            if folder.parent:
                folder = folder.parent
        except Exception as e:
            logger.exception(e)
            # in some cases, you have no read permission, it will give more detail with error code
            if e.error_code == "AuthorizationPermissionMismatch":
                raise RuntimeError(e.error_code)
            exit(1)
    
    with open(FOLDERS, "wb") as f:
        pickle.dump(result, f)
    
    sess.commit()
    Session_local.remove()
    return result

def load_page(cid, pid, page):
    """
    Process a page of blobs.

    args:
        cid: container id
        pid: blob's parent id
        page: PageItem of BlobProperty
    return:
        items, size, folder_mtime
    """

    items = []

    size = 0
    folder_mtime = datetime.datetime(1900,1,1)
    for item in page:
        size += item.size
        mtime = item.last_modified.astimezone(utc).replace(tzinfo=None)
        folder_mtime = mtime if mtime > folder_mtime else folder_mtime
        items.append({
            "path": item.name,
            "size": item.size,
            "mtime": mtime,
            "container_id": cid,
            "parent_id": pid
        })
    return items, size, folder_mtime

def list_folder(container_client, cid, resume, Session_local, folder):
    """
    Get blobs infomation under the folder and update to the database.

    args:
        container_client, cid, resume, Session_local, folder
    return:
        state (error 1 success 0)
    """
    thread_id = current_thread().ident%10**5
    if resume and folder.is_completed:
        logger.success(f"<{thread_id}>: {container_client.account_name}::{container_client.container_name}/{folder.path} has completed, skipping.")
        return 0
    global err_count
    prefix = folder.path
    pid = folder.id
    token = folder.token[0].continuation_token if resume and folder.token else None
    pages = container_client.list_blobs(name_starts_with=prefix).by_page(token)
    pages.results_per_page = MAX_PAGE_SIZE

    sess = Session_local()
    logger.info(f"<{thread_id}>: {container_client.account_name}::{container_client.container_name} Start scanning F: {prefix}")
    has_error = 0
    checkpoint = Checkpoint(folder_id=pid, continuation_token=None)
    with mutex:
        checkpoint = query_insert_data(sess, checkpoint)
    
    if not resume:
        with mutex:
            sess.query(Blob).filter_by(parent_id=pid).delete()
            sess.commit()
    
    while (True):
        try:
            retry = 0
            success = False
            while not success and retry < MAX_RETRY_TIMES:
                try:
                    page = func_timeout.func_timeout(TIMEOUT, pages.next)
                    success = True
                except func_timeout.exceptions.FunctionTimedOut:
                    retry += 1
                    logger.warning(f"<{thread_id}>: F: {container_client.account_name}::{container_client.container_name}/{prefix} Get next page timeout. Retried {retry} times.")
            if success:
                blobs, size, folder_mtime = load_page(cid, pid, page)
                logger.info(f"<{thread_id}>: Found {len(blobs)} blobs in {container_client.account_name}::{container_client.container_name}/{prefix}")
                with mutex:
                    # Deprecated, try storing blobs info, but use too much space
                    # sess.bulk_insert_mappings(Blob, blobs)
                    checkpoint.continuation_token = pages.continuation_token
                    sess.query(Folder).filter_by(id=pid).update({Folder.size: Folder.size+size, Folder.blobs_count:Folder.blobs_count+len(blobs),Folder.mtime: folder_mtime})
                    sess.commit()
            else:
                with err_lock:
                    err_count += 1
                has_error = 1
                logger.error(f"<{thread_id}>: F: {prefix} scan Failed. Get next page timeout {retry} times.")
                break
        except StopIteration:
            with mutex:
                sess.query(Folder).filter_by(id=pid).update({Folder.is_completed: True})
                sess.commit()
            logger.success(f"<{thread_id}>: F: {prefix} scan success.") 
            break
        except Exception as e:
            logger.exception(e)
            exit(1)
    Session_local.remove()
    return has_error

def compute_tree(container_client, cid, prefix, max_depth, Session_local):
    """After getting subfolders storage info, compute the folder tree infomation recursively.
    """
    sess = Session_local()
    # stk is a DFS state stack, store iterators of the blob folders
    stk = []
    # every folder will be initialized to zero, `walked` stack ensures initialization once only
    walked = []
    stk.append(container_client.walk_blobs(name_starts_with=prefix))
    walked.append(True)
    folder = sess.query(Folder).filter_by(container_id=cid, path=prefix).first()
    if folder.is_completed:
        logger.success(f'{container_client.account_name}::{container_client.container_name} root {prefix} has done. Skipping')
        return
    folder.size = 0
    folder.blobs_count = 0
    logger.info(f'{container_client.account_name}::{container_client.container_name} Computing tree nodes with root {prefix}...')
    depth = 1
    mdepth = max_depth
    while stk:
        try:
            item = stk[-1].next()
            if depth <= mdepth and not walked[-1]:
                folder.size = 0
                folder.blobs_count = 0
                walked[-1] = True
            if isinstance(item, BlobPrefix):
                logger.info('F: ' +item.name)
                subfolder = Folder(container_id=cid, path=item.name, parent_id=folder.id)
                subfolder = query_insert_data(sess, subfolder)
                if depth < mdepth:
                    depth += 1
                    folder = subfolder
                    stk.append(item)
                    walked.append(False)
            else:
                while(isinstance(item, BlobProperties)):
                    logger.info('B: '+item.name)
                    mtime = item.last_modified.astimezone(utc).replace(tzinfo=None)
                    folder.size += item.size
                    folder.mtime = mtime if mtime > folder.mtime else folder.mtime
                    folder.blobs_count += 1
                    # blob = Blob(path=item.name, size=item.size, mtime=mtime, container_id=cid, parent_id=folder.id)
                    # blob = query_insert_data(sess, blob)
                    item = stk[-1].next()
        except StopIteration:
            stk.pop()
            walked.pop()
            depth -= 1
            all_success = True
            for subfolder in folder.subfolders:
                add_subfolder(sess, folder, subfolder)
                if not subfolder.is_completed:
                    all_success = False
            if all_success:
                folder.is_completed = True
                sess.commit()
            if folder.parent:
                folder = folder.parent
        except Exception as e:
            logger.exception(e)
            exit(1)
    Session_local.remove()

def walk_container(container_client, prefix, Session_local=Session_local, resume=True, sub=None):
    """
    Walk the folder tree of the container.

    Build folder tree, and scan every subfolder, finally compute the folder tree.
    """
    sess = Session_local()
    cid = sess.query(Container.id).filter_by(account_name=container_client.account_name, name=container_client.container_name).first()[0]
    try:
        folders_id = build_tree(container_client, cid, prefix, Session_local, MAX_DEPTH, resume, sub)
    except RuntimeError as e:
        logger.exception(e)
        return

    logger.info(f"{container_client.account_name}::{container_client.container_name} Found {len(folders_id)} subfolders with depth={MAX_DEPTH}")
    if resume:
        root = sess.query(Folder).filter_by(container_id=cid, path="").first()
        if root and root.is_completed:
            logger.info("Container completed. Skipping.")
            return
            
    folders = []
    for folder_id in folders_id:
        folders.append(sess.query(Folder).filter_by(id=folder_id).first())

    # # MultiThreading
    with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        results = list(tqdm(executor.map(partial(list_folder, container_client, cid, resume, Session_local), folders),
                            total=len(folders)))
    # # Convert to Multithreading
    # for folder in folders:
    #     list_folder(container_client, cid, resume, folder)
    # for root only
    if not folders:
        logger.info(f"{container_client.account_name}::{container_client.container_name} There is no subfolders.")
        root = sess.query(Folder).filter_by(container_id=cid, path=prefix).first()
        list_folder(container_client, cid, resume, Session_local, root)
    else:
        compute_tree(container_client, cid, prefix, MAX_DEPTH, Session_local)

    logger.info(f"Scanned {len(folders)} subfolders in total, {err_count} failed.")

    Session_local.remove()
        
    
