# Build database model and offer some method
from sqlalchemy import Column, create_engine, ForeignKey
from sqlalchemy import Boolean, String, Integer, BigInteger, DateTime
from sqlalchemy.orm import sessionmaker, relationship, scoped_session, backref
from sqlalchemy.ext.declarative import declarative_base
import datetime
import logging
from loguru import logger

from tools import bytes_unit_convert
from config import configs

logging.basicConfig(filename='logger.log', level=logging.INFO)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

default_datetime = datetime.datetime(1900,1,1)
DB_name = configs['database_name']
engine = create_engine(f"sqlite+pysqlite:///{DB_name}.db?check_same_thread=False", encoding="utf-8", future=True)

Base =  declarative_base()
Session = sessionmaker(bind=engine)
session = Session()

# Container
# name num_blobs total_size
class Container(Base):
    """
    Table: container
    Cols:
        id
        account_name(required)
        name(requied)
        blobs_count
        total_size
    backrefs:
        folders
        blobs
    """
    __tablename__ = 'container'

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_name = Column(String, nullable=False)
    name = Column(String, nullable=False)
    blobs_count = Column(Integer, default=0)
    total_size = Column(BigInteger, default=0)

    def __repr__(self):
        return f"Container account_name:{self.account_name!r}, name:{self.name!r}"

    @property
    def c_size(self):
        return bytes_unit_convert(total_size)
# Folder
# container path last_modified total_size blobs folders
# list_of_paths list_of_files   is_completed?
class Folder(Base):
    """
    Table: folder
    Cols:
        id
        container_id(required, foreign key)
        path(required)
        size
        mtime
        blobs_count
        is_completed
        parent_id(foreign key)
    refs:
        container
        parent
    backrefs:
        subfolders
        blobs
    """
    __tablename__ = 'folder'

    id = Column(Integer, primary_key=True, autoincrement=True)
    container_id = Column(Integer, ForeignKey('container.id'))
    path = Column(String, nullable=False)
    size = Column(BigInteger, default=0)
    mtime = Column(DateTime, default=default_datetime)
    blobs_count = Column(Integer, default=0)
    is_completed = Column(Boolean, default=False)
    parent_id = Column(Integer, ForeignKey("folder.id"))

    container = relationship("Container", backref="folders")
    parent = relationship("Folder", remote_side=[id], backref="subfolders")
    token = relationship("Checkpoint", backref=backref("folder", uselist=False))
    
    def __repr__(self):
        return f"Folder path:{self.path!r}\tsize:{self.c_size!r}\tblobs:{self.blobs_count!r}\tlast_modified:{self.c_mtime!r}\tis_completed:{self.is_completed!r}"
    
    @property
    def c_mtime(self):
        return self.mtime.strftime("%x %X")

    @property
    def c_size(self):
        return bytes_unit_convert(self.size)

# Blobs
# container path last_modified size 
class Blob(Base):
    """
    Table: blob
    Cols:
        id
        path(required)
        size(required)
        mtime(required)
        container_id(required, foreignkey)
        parent_id(required, foreignkey)
    refs:
        container
        parent
    """
    __tablename__ = 'blob'

    id = Column(Integer, primary_key=True, autoincrement=True)
    path = Column(String, nullable=False)
    size = Column(Integer, default=0)
    mtime = Column(DateTime, default=default_datetime)
    container_id = Column(Integer, ForeignKey('container.id'))
    parent_id = Column(Integer, ForeignKey('folder.id'))

    container = relationship("Container", backref="blobs")
    parent = relationship("Folder", backref="blobs")

    def __repr__(self):
        return f"Blob path:{self.path!r}\tsize:{self.c_size!r}\tlast_modified:{self.c_mtime!r}"
    @property
    def c_mtime(self):
        return self.mtime.strftime("%x %X")

    @property
    def c_size(self):
        return bytes_unit_convert(self.size)

class Checkpoint(Base):
    """
    Table:
        checkpoint (save continuation token)
    Cols:
        folder_id
        continuation_token
    """
    __tablename__ = 'checkpoint'
    folder_id = Column(Integer, ForeignKey(Folder.id), primary_key=True)
    continuation_token = Column(String)

    def __repr__(self):
        return f"Checkpoint {self.folder_id!r}\t{self.continuation_token!r}"

Base.metadata.create_all(engine)



def insert_data(sess, cls):
    sess.add(cls)
    sess.commit()
    return cls


def query_insert_data(sess, obj):
    if isinstance(obj, Folder):
        res = sess.query(Folder).filter_by(container_id=obj.container_id, path=obj.path).first()
    elif isinstance(obj, Container):
        res = sess.query(Container).filter_by(account_name=obj.account_name, name=obj.name).first()
    elif isinstance(obj, Blob):
        res = sess.query(Blob).filter_by(container_id=obj.container_id, path=obj.path).first()
        if res:
            res.size = obj.size
            res.mtime = obj.mtime
    elif isinstance(obj, Checkpoint):
        res = sess.query(Checkpoint).filter_by(folder_id=obj.folder_id).first()
    else:
        logger.error(str(obj)+" is not legal object!")
        exit(1)
    
    return res if res else insert_data(sess, obj)
