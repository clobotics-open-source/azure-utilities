configs = {
    # 'file': None, 
    # 'secrets': None, 
    # 'cli': False
    'database_name': 'test.db', 
    'force_restart': False, 
    'max_depth': 2, 
    'max_workers': None,
    'page_size': 5000, 
    'retry_times': 3,
    'timeout': 60,
    'display_only': False,
    'limit': 20
}