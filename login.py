from azure.storage.blob import BlobServiceClient, ContainerClient
import json
import os
from loguru import logger
from database import Session, Container, query_insert_data


def getenv(envstr, fault=True):
    """ Return the environment variable in envstr.  If environment is not set and fault is True, then exit with error."""
    envval = os.environ.get(envstr)
    if envval is None:
        logger.error(f"Missing environment variable: {envstr}")
        if fault:
            exit(1)

    return envval

def login(info):
    """
    login az storage account and save container info to database
    """
    try:
        logger.info("AZ login with "+info['type'])
        if info['type'] == "SAS":
            url_sas =  info['base_url'] + getenv(info['env_name']) if "env_name" in info else info['secrets']
            blob_srv_client = BlobServiceClient(url_sas)
        else:
            conn_str = getenv(info['env_name']) if "env_name" in info else info['secrets']
            blob_srv_client = BlobServiceClient.from_connection_string(conn_str)
    except:
        logger.exception("Login Faild!")
        exit(1)
    
    register(blob_srv_client)
    
    return blob_srv_client

def register(blob_srv_client, Session=Session):
    """
    register containers info of storage account
    """
    sess = Session()
    account_name = blob_srv_client.account_name
    container_list = list(blob_srv_client.list_containers())
    logger.info(f"Found {len(container_list)} containers.")
    for c in container_list:
        container = Container(account_name=account_name, name=c.name)
        container = query_insert_data(sess, container)

