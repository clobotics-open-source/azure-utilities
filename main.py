# main
import json
from loguru import logger
import time
import argparse

from config import configs

def main():
    # Read command line arguments
    parser = argparse.ArgumentParser(description="Get Azure storage blob usage infomation.")
    parser.add_argument("-f", "--file", help="Path to json file with containers info.")
    parser.add_argument("-s", "--secrets", help="Connection string or URL with SAS, used to login storage account.")
    parser.add_argument("-c", "--cli", action="store_true", help="Use Azure Cli to log in.")
    parser.add_argument("--force-restart", action="store_true", help="Force to restart from scratch.")
    parser.add_argument("-w", "--max-workers", type=int, default=None, help="Max workers used by multithreading.")
    parser.add_argument("-d", "--max-depth", type=int, default=2, help="Max search tree depth with walk_blobs method.")
    parser.add_argument("-t", "--timeout", type=int, default=30, help="Timeout of list_blobs requests.")
    parser.add_argument("-r", "--retry-times", type=int, default=3, help="Retry times when requests timeout.")
    parser.add_argument("-p", "--page-size", type=int, default=2500, help="Number of blobs requested once.")
    parser.add_argument("-n", "--database-name", default="test", help="Sqlite database file path.")
    parser.add_argument("-l", "--limit", default=20, help="The number of items when display.")
    parser.add_argument("--display-only", action="store_true", help="Display the middle results when scanning.")
    args = parser.parse_args()

    for key, value in args._get_kwargs():
        configs[key] = value

    logger.add("logs/main_log_{time}.log", rotation="500MB", encoding="utf-8", enqueue=True, compression="zip", retention="1 days")
    
    container_list = []
    if args.file:
        try:
            container_list = json.load(open(args.file))['list']
        except Exception as e:
            logger.exception(e)
    if args.secrets:
        info['secrets'] = args.secrets
        info['container_name'] = ""
        if args.secrets.startswith("http"):
            info['type'] = 'SAS'
        else:
            info['type'] = "connection_string"
        container_list.append(info)

    if not container_list and not args.cli:
        logger.warning("No json file or secrets argument provided!")
        exit(1)

    from scan import walk_container
    from login import login
    from display import display

    resume = not args.force_restart

    if args.cli:
        from cli import run_from_cli
        run_from_cli()
        exit(0)

    for info in container_list:
        blob_srv_client = login(info)
        account_name = blob_srv_client.account_name
        if info['container_name']:
            container_name = info['container_name']
            if args.display_only:
                display(account_name, container_name)
                continue
            prefix = info['prefix'] if 'prefix' in info else ""
            container_client = blob_srv_client.get_container_client(container_name)
            begin_time = time.time()
            walk_container(container_client, prefix, resume=resume)
            stop_time = time.time()
            logger.info(f"{account_name}/{container_name}: Consume {stop_time-begin_time} seconds.")
            display(account_name, container_name, prefix)
        else:
            for container in blob_srv_client.list_containers():
                container_name = container.name
                if args.display_only:
                    display(account_name, container_name)
                    continue
                prefix = ""
                container_client = blob_srv_client.get_container_client(container_name)
                begin_time = time.time()
                walk_container(container_client, prefix, resume=resume)
                stop_time = time.time()
                logger.info(f"{account_name}/{container_name}: Consume {stop_time-begin_time} seconds")
                display(account_name, container_name, prefix)


if __name__=="__main__":
    main()
