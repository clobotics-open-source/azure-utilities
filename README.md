# Azure blob storage probe

一个用来探查Azure Blob容器使用情况的工具，使用Azure Python SDK逐文件夹探查blob，并使用SQLAlchemy将信息写入到数据库，最终展示各个文件夹及文件的存储空间占用。支持中断后断点继续执行。

## Usage

Step1. Install required Packages
```
pip install -r requirements.txt
```

Step2. Set up SAS or CONN environmental variables

```bash
export $YOUR_ACCOUNT_NAME_SAS="xxx"
# or
export $YOUR_ACCOUNT_NAME_CONN="xxx"
```

Step3. Config secrets.json

```json
// secrets.json
{
    "list": [
        {
            "type": "SAS",
            "container_name": "name", // set a spcified name
            "prefix": "folder/", // you can also specify a prefix
            "base_url":"https://xx.blob.core.windows.net/", // required when using SAS
            "env_name": "xx_SAS"
        },
        {
            "type": "connection_string",
            "container_name": "",  // or set a null string for all container
            "env_name": "xx_CONN"
        }
    ]
}
```

Step4. Run main.py
```
python main.py -f secrets.json
```
or
```
python main.py -s <URLwithSAS>
python main.py -s <Connection_string>
```

Step5. Display results

When scanning over, it will display the result. And you can also run
```
python main.py -f secrets.json --display-only
```
to display middle results.

## Modules

- main.py
  - 程序主入口
- scan.py
  - 使用DFS遍历容器，将走过的路径存入数据库，计算总大小等信息
- database.py
  - 建立数据库模型，使用SQLAlchemy ORM操纵数据库
  - 数据表：
    - container
    - folder 
    - blob
    - checkpoint
- display.py
  - 查询数据库，展示统计信息
- tools.py
  - 单位换算工具
- secrets.json
  - 用于记录要探查容器名称，以及SAS或连接字符串环境变量名。

