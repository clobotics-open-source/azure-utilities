import os
import json
import subprocess
import sys
from concurrent import futures

from azure.common.credentials import get_cli_profile
from knack.util import CLIError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from azure.identity import AzureCliCredential
from loguru import logger

from database import Container, Base, query_insert_data
from display import display, display_sub
from config import configs
from azure.storage.blob import BlobServiceClient
from login import register

_GREEN = "\033[0;32m"
_BOLD = "\033[;1m"

def _run_az_cli_login():
    """If not logged in az cli, run 'az login' firstly.
    """
    process = subprocess.Popen(
        ["az", "login"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    for c in iter(lambda: process.stdout.read(1), b""):
        sys.stdout.write(_GREEN + _BOLD + c.decode(sys.stdout.encoding))

def list_subscriptions():
    """Use az cli command 'az account list' to get subscription info.
    """
    logger.info("Listing subscriptions from cli...")
    sub_info = json.load(os.popen("az account list"))
    if not sub_info:
        logger.info("Not logged in or No available subscription, running az login")
        _run_az_cli_login()
        sub_info = json.load(os.popen("az account list"))

    return [["Subscription_name", "Subscription ID"]] + [
        [sub['name'], sub['id']]
        for sub in sub_info
    ]

def select(sub_name_or_id):
    """Select cli active subscription by subscription name or id.
    """
    profile = get_cli_profile()
    profile.set_active_subscription(sub_name_or_id)

def get_accounts():
    """
    Get all storage accounts in all subscriptions.

    Return a list of storage accounts divided by subscription.
    """
    subs = list_subscriptions()
    res = []
    logger.info("Getting storage account using cmd.")
    for sub in subs[1:]:
        select(sub[0])
        res.append([sub[0]])
        info = os.popen("az storage account list")
        try:
            for item in json.load(info):
                res[-1].append(item['primaryEndpoints']['blob'])
        except Exception as e:
            logger.exception(e)
            exit(1)
        logger.info(f"Found {len(res[-1])} storage accounts in subscription {sub[0]}.")
    return res

def walk_account(subscription, account):
    """
    Get infomation of the storage account. 

    List containers under the storage account, walk containers and display infomation.
    """
    from scan import walk_container

    global acc_sess

    logger.info(f"Processing {subscription}  {account}")
    
    sessmaker = acc_sess[subscription][account]
    resume = not (configs.get('force_restart', False))
    azure_cli = AzureCliCredential()

    blob_srv_client = BlobServiceClient(account, credential=azure_cli)
    register(blob_srv_client, sessmaker)
    account_name = blob_srv_client.account_name
    containers = list(blob_srv_client.list_containers())
    logger.info(f"There are {len(containers)} Containers under {subscription}  {account}.")

    for container in containers:
        container_name = container.name
        prefix = ""
        container_client = blob_srv_client.get_container_client(container_name)
        walk_container(container_client, prefix, sessmaker, resume, subscription)
        display(account_name, container_name, prefix, sessmaker)

class AccountsSess(dict):
    """
    Build a database for every storage account, this dict maps storage accounts to SQLAlchemy sessions.
    
    SQLAlchemy session can be acquired by 
        AccountSess[subscription_name][storage_account]
    """
    def __init__(self):
        if not os.path.isdir("databases"):
            os.mkdir("databases")
        sub_accounts = get_accounts()        
        for sub in sub_accounts:
            sub_name = sub.pop(0)
            if not os.path.isdir("databases/"+sub_name):
                os.mkdir("databases/"+sub_name)
            self[sub_name] = {}
            for account in sub:
                db_name = account.split('/')[2].replace(".", "_")
                engine = create_engine(f"sqlite+pysqlite:///databases/{sub_name}/{db_name}.db?check_same_thread=False", encoding="utf-8", future=True)
                sess = scoped_session(sessionmaker(bind=engine))
                Base.metadata.create_all(engine)
                self[sub_name][account] = sess

acc_sess = AccountsSess()

def run_from_cli():
    """
    Main function of processing available storage accounts with az cli.
    
    Multiprocess are introduced to accelerate.
    """
    global acc_sess
    logger.info(f"Starting processing {len(acc_sess)} accounts.")
    # MultiProcess
    for sub in acc_sess:
        logger.info(f"Executing {sub}")
        if configs.get('display_only', False):
            display_sub(sub, acc_sess[sub])
            continue
        with futures.ProcessPoolExecutor(max_workers=None) as executor:
            res = [executor.submit(walk_account, sub, acc) for acc in acc_sess[sub]]
            futures.wait(res)
        logger.info(f"Outputing result of {sub}")
        display_sub(sub, acc_sess[sub])

    # # Single Process
    # for sub in acc_sess:
    #     for acc in acc_sess[sub]:
    #         walk_account(sub, acc)
    #     display_sub(sub, acc_sess[sub])

    logger.info("All account executed over.")

if __name__ == '__main__':
    logger.add("logs/cli_log_{time}.log", rotation="500MB", encoding="utf-8", enqueue=True, compression="zip", retention="1 days")
    run_from_cli()