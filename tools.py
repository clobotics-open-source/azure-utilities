import math
units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'] # 1EB = (2^10)^6B Z Y B N D C X...
def bytes_unit_convert(size):
    if size < 1024:
        return f"{size}B"
    unit = int(math.log2(size)/10)
    res = size/2**(10*unit)
    
    return f"{res:.2f}{units[unit]}" 

        